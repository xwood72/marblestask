package com.company.MarblesAndHolesGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    static int tableSize = 0;
    static int nBalls = 0;
    static int nHoles = 0;
    static int nWalls = 0;
    static List<Hole> listHoles = new ArrayList<>();
    static List<Ball> listBalls = new ArrayList<>();
    static GameBoard board;

    public static void main(String[] args) {
	// write your code here
        String input= "4 2 2 2 2 1 4 1 1 4 3 1 2 1 3 3 2 4 2";
        String[] params = input.trim().split(" ");

        parseParams(params);

//        GameBoard b = SolutionFinder.inclineToEast(board);
//        GameBoard b = SolutionFinder.inclineToWest(board);
//        GameBoard b = SolutionFinder.inclineToNorth(board);
//        GameBoard b = SolutionFinder.inclineToSouth(board);
//        b.toString();
//        b = SolutionFinder.inclineToWest(b);
//        b.toString();
//        b = SolutionFinder.inclineToEast(b);
//        b.toString();

        SolutionFinder.findSolution(board);
//        GameBoard b = SolutionFinder.inclineToNorth(board);
//        b.toString();
//        b = SolutionFinder.inclineToEast(board);
//        b.toString();
//        b = SolutionFinder.inclineToWest(board);
//        b.toString();



        System.out.println("END");
    }

    private static void parseParams(String[] params){
        //TODO добавить валидацию параметров
        System.out.println("params: " + Arrays.toString(params));
        tableSize = Integer.parseInt(params[0]);
        nBalls = Integer.parseInt(params[1]);
        nHoles = Integer.parseInt(params[1]);
        nWalls = Integer.parseInt(params[2]);

        System.out.println("Размер доски: " + tableSize);
        System.out.println("Количество шаров: " + nBalls);
        System.out.println("Количество дырок: " + nHoles);
        System.out.println("Количество стен: " + nWalls);

        board = new GameBoard(tableSize);

        //Инициализация шаров
        for (int i = 3; i < 3 + nBalls * 2; i += 2) {
            int x = Integer.parseInt(params[i]);
            int y = Integer.parseInt(params[i+1]);
            Ball b = new Ball( (i - 3) / 2, x-1, y-1);
            listBalls.add(b);
            System.out.println("Шар x=:" + b.getX() + " y=" + b.getY());
            board.addBall(b);
        }


        //Инициализация дырок
        for (int i = 3 + nBalls * 2; i < 3 + nHoles * 2 + nWalls * 2 ; i += 2) {
            int x = Integer.parseInt(params[i]);
            int y = Integer.parseInt(params[i+1]);
            Hole h = new Hole((i - 3 - nBalls * 2) / 2, x-1, y-1);
            listHoles.add(h);
            System.out.println("Дырка x=:" + h.getX() + " y=" + h.getY());
            board.addHole(h);
        }

        //Инициализация стен
        int startIndex = 3 + nHoles * 2 + nBalls * 2;
        for (int i = startIndex; i < startIndex + nWalls * 4; i += 4) {
            int x1 = Integer.parseInt(params[i]) - 1;
            int y1 = Integer.parseInt(params[i + 1]) - 1;
            int x2 = Integer.parseInt(params[i + 2]) - 1;
            int y2 = Integer.parseInt(params[i + 3]) - 1;
            System.out.println("Стена между ячейками: [" + x1 + ", " + y1
                +  "] , [" + x2 + ", " + y2 + "]");
            board.addWall(x1, y1, x2, y2);
        }
    }


}
