package com.company.MarblesAndHolesGame;

/**
 * Created by xw on 01.08.17.
 */
public class Hole {

    private int id;
    private int x;
    private int y;

    public Hole(int id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getId() {
        return id;
    }
}
