package com.company.MarblesAndHolesGame;

/**
 * Created by xw on 01.08.17.
 */
public class Cell implements Cloneable {

    private boolean isSouthBlocked;
    private boolean isEastBlocked;
    private boolean isNorthBlocked;
    private boolean isWestBlocked;

    private Hole hole;
    private Ball ball;

    public Cell() {
    }

    public boolean isSouthBlocked() {
        return isSouthBlocked;
    }

    public void setSouthBlocked(boolean southBlocked) {
        this.isSouthBlocked = southBlocked;
    }

    public boolean isEastBlocked() {
        return isEastBlocked;
    }

    public void setEastBlocked(boolean eastBlocked) {
        this.isEastBlocked = eastBlocked;
    }

    public boolean isNorthBlocked() {
        return isNorthBlocked;
    }

    public void setNorthBlocked(boolean northBlocked) {
        this.isNorthBlocked = northBlocked;
    }

    public boolean isWestBlocked() {
        return isWestBlocked;
    }

    public void setWestBlocked(boolean westBlocked) {
        this.isWestBlocked = westBlocked;
    }

    public Hole getHole() {
        return hole;
    }

    public void setHole(Hole hole) {
        this.hole = hole;
    }

    public Ball getBall() {
        return ball;
    }

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    public boolean isEmpty(){
        if (hole == null && ball == null){
            return true;
        } else return false;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Cell clone = new Cell();
        clone.setNorthBlocked(isNorthBlocked);
        clone.setEastBlocked(isEastBlocked);
        clone.setSouthBlocked(isSouthBlocked);
        clone.setWestBlocked(isWestBlocked);

        if (this.hole != null){
            int id = this.hole.getId();
            int x = this.hole.getX();
            int y = this.hole.getY();
            Hole h = new Hole(id, x, y);
            clone.setHole(h);
        }

        if (this.ball != null){
            int id = this.ball.getId();
            int x = this.ball.getX();
            int y = this.ball.getY();
            Ball b = new Ball(id, x, y);
            clone.setBall(b);
        }

        return clone;
    }
}
