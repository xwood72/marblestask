package com.company.MarblesAndHolesGame;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xw on 01.08.17.
 */
public class GameBoard {

    private Cell[][] gameBoard;
    private List<Hole> holes = new ArrayList<>();
    private List<Ball> balls = new ArrayList<>();
    private int size;
    private String turnsCombination = "";

    public GameBoard(GameBoard board) {
        this.size = board.getSize();

//        this.gameBoard = board.getGameBoard();
        this.gameBoard = new Cell[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                try {
                    this.gameBoard[i][j] = (Cell) board.getGameBoard()[i][j].clone();
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            }
        }

//        this.holes = board.getHoles();
        for (Hole h: board.getHoles()){
            this.holes.add(h);
        }

//        this.balls = board.getBalls();
        for (Ball b: board.getBalls()){
            this.balls.add(b);
        }

        this.turnsCombination = new String(board.getTurnsCombination());
    }

    public GameBoard(int size) {
        this.size = size;
        gameBoard = new Cell[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                gameBoard[i][j] = new Cell();
            }

        }
    }

    public void addHole(Hole h) {
        (gameBoard[h.getX()][h.getY()]).setHole(h);
        holes.add(h);
    }

    public void removeHole(Hole h) {
        (gameBoard[h.getX()][h.getY()]).setHole(null);
        Hole cH = null;
        holes.remove(h);
        for (Hole curH: holes){
            if (curH.getId() == h.getId()){
                cH = curH;
            }
        }
        holes.remove(cH);
    }

    public void addBall(Ball b) {
        int x = b.getX();
        int y = b.getY();
        gameBoard[x][y].setBall(b);
        balls.add(b);
    }

    public void removeBall(Ball b) {
        Ball cB = null;
        for (Ball curB: balls){
            if (curB.getId() == b.getId()){
                cB = curB;
            }
        }
        balls.remove(cB);
    }

    public void addWall(int x1, int y1, int x2, int y2) {
        if (x1 == x2) {
            if (y1 < y2) {
                gameBoard[x1][y1].setSouthBlocked(true);
                gameBoard[x2][y2].setNorthBlocked(true);
            } else {
                gameBoard[x2][y2].setSouthBlocked(true);
                gameBoard[x1][y1].setNorthBlocked(true);
            }
        }

        if (y1 == y2) {
            if (x1 < x2) {
                gameBoard[x1][y1].setEastBlocked(true);
                gameBoard[x2][y2].setWestBlocked(true);
            } else {
                gameBoard[x2][y2].setEastBlocked(true);
                gameBoard[x1][y1].setWestBlocked(true);
            }
        }
    }

    public List<Hole> getHoles() {
        return holes;
    }

    public List<Ball> getBalls() {
        return balls;
    }

    public int getSize() {
        return size;
    }

    public Cell[][] getGameBoard() {
        return gameBoard;
    }

    public Cell getCell(int x, int y) {
        return gameBoard[x][y];
    }

    public String getTurnsCombination() {
        return turnsCombination;
    }

    public void setTurnsCombination(String turnsCombination) {
        this.turnsCombination = turnsCombination;
    }

    public int moveToRight(Ball b) {
        int bX = b.getX();
        int bY = b.getY();
        int newX = bX;
        while (!gameBoard[newX][bY].isEastBlocked() &&
                gameBoard[newX][bY].getHole() == null &&
                newX < size - 1) {

            newX++;
            if (gameBoard[newX][bY].getBall() != null) break;
        }

        //Шарик закатился в свою дырку
        Hole h = gameBoard[newX][bY].getHole();
        if (h != null) {
            if (h.getId() == b.getId()) {
                removeBall(b);
                removeHole(h);
                gameBoard[bX][bY].setBall(null);
                gameBoard[newX][bY].setHole(null);
            } else {
                return -1;
            }
        } else {
            gameBoard[bX][bY].setBall(null);
            removeBall(b);
            Ball newBall = new Ball(b.getId(), newX, bY);
            addBall(newBall);
        }


        return 0;
    }

    public int moveToLeft(Ball b) {
        int bX = b.getX();
        int bY = b.getY();
        int newX = bX; //newX - новая координата X для шарика

        //ищем ячейку, в которую сдвигается шарик
        while (!gameBoard[newX][bY].isWestBlocked() &&
                gameBoard[newX][bY].getHole() == null &&
                newX > 0) {

            newX--;
            if (gameBoard[newX][bY].getBall() != null){
                newX++;
                break;
            }
        }

        //Шарик закатился в свою дырку
        Hole h = gameBoard[newX][bY].getHole();
        if (h != null) {
            if (h.getId() == b.getId()) {
                removeBall(b);
                removeHole(h);
                gameBoard[bX][bY].setBall(null);
                gameBoard[newX][bY].setHole(null);
            } else {
                return -1;
            }
        } else {
            gameBoard[bX][bY].setBall(null);
            removeBall(b);
            Ball newBall = new Ball(b.getId(), newX, bY);
            addBall(newBall);
        }
        return 0;
    }

    public int moveToUp(Ball b) {
        int bX = b.getX();
        int bY = b.getY();
        int newY = bY; //newY - новая координата Y для шарика

        //ищем ячейку, в которую сдвигается шарик
        while (!gameBoard[bX][newY].isNorthBlocked() &&
                gameBoard[bX][newY].getHole() == null &&
                newY > 0) {

            newY--;
            if (gameBoard[bX][newY].getBall() != null) break;
        }

        //Шарик закатился в свою дырку
        Hole h = gameBoard[bX][newY].getHole();
        if (h != null) {
            if (h.getId() == b.getId()) {
                removeBall(b);
                removeHole(h);
                gameBoard[bX][bY].setBall(null);
                gameBoard[bX][newY].setHole(null);
            } else {
                return -1;
            }
        } else {
            gameBoard[bX][bY].setBall(null);
            removeBall(b);
            Ball newBall = new Ball(b.getId(), bX, newY);
            addBall(newBall);
        }
        return 0;
    }

    public int moveToDown(Ball b) {
        int bX = b.getX();
        int bY = b.getY();
        int newY = bY; //newY - новая координата Y для шарика

        //ищем ячейку, в которую сдвигается шарик
        while (!gameBoard[bX][newY].isNorthBlocked() &&
                gameBoard[bX][newY].getHole() == null &&
                newY < size - 1) {

            newY++;
            if (gameBoard[bX][newY].getBall() != null) break;
        }

        //Шарик закатился в свою дырку
        Hole h = gameBoard[bX][newY].getHole();
        if (h != null) {
            if (h.getId() == b.getId()) {
                removeBall(b);
                removeHole(h);
                gameBoard[bX][bY].setBall(null);
                gameBoard[bX][newY].setHole(null);
            } else {
                return -1;
            }
        } else {
            gameBoard[bX][bY].setBall(null);
            removeBall(b);
            Ball newBall = new Ball(b.getId(), bX, newY);
            addBall(newBall);
        }
        return 0;
    }

    public boolean isWin(){
        if (balls.size() == 0 && holes.size() == 0){
            return true;
        } else return false;
    }


    @Override
    public String toString() {
        System.out.println("\nРазмер доски: " + size);
        for (Ball b : balls) {
            System.out.println("Шарик: [" + b.getX() + ", " + b.getY() + "]");
        }
        for (Hole h : holes) {
            System.out.println("Дырка: [" + h.getX() + ", " + h.getY() + "]");
        }
        System.out.println("Turns combination: " + turnsCombination);
        return super.toString();
    }


}
