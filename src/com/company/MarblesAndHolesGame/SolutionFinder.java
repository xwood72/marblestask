package com.company.MarblesAndHolesGame;

import java.util.*;

/**
 * Created by xw on 02.08.17.
 */
public class SolutionFinder {

    private static Queue<GameBoard> boardStates = new ArrayDeque<>();

    public static String findSolution(GameBoard board) {
        String winCombination = "";
        if (board.isWin()) return "";
        boardStates.add(board);
        int deep = 0;
        List<String> winCombs = new ArrayList<>();
        while (!boardStates.isEmpty() && deep < 2000) {
            deep++;
            GameBoard b = new GameBoard(boardStates.remove());
            for (int i = 1; i <= 4; i++) {
                switch (i) {
                    case 1:
                        GameBoard b1 = inclineToNorth(b);
                        b1.toString();
                        if (b1.isWin()) {
                            winCombs.add(b1.getTurnsCombination());
                        }
                        boardStates.add(b1);
                        break;
                    case 2:
                        GameBoard b2 = inclineToEast(b);
                        b2.toString();
                        if (b2.isWin()) {
                            winCombs.add(b2.getTurnsCombination());
                        }
                        boardStates.add(b2);
                        break;
                    case 3:
                        GameBoard b3 = inclineToSouth(b);
                        b3.toString();
                        if (b3.isWin()) {
                            winCombs.add(b3.getTurnsCombination());
                        }
                        boardStates.add(b3);
                        break;
                    case 4:
                        GameBoard b4 = inclineToWest(b);
                        b4.toString();
                        if (b4.isWin()) {
                            winCombs.add(b4.getTurnsCombination());
                        }
                        boardStates.add(b4);
                        break;
                }
            }
//            if (winCombs.size() > 0) break;
        }
        System.out.println(Arrays.toString(winCombs.toArray()) + " Finish!");
        return winCombs.toArray() + " Finish!";
    }


    public static GameBoard inclineToEast(GameBoard board) {
        int size = board.getSize();
        GameBoard newBoard = new GameBoard(board);
        newBoard.setTurnsCombination(board.getTurnsCombination() + "E");
        for (int row = 0; row <= size - 1; row++) {
            for (int col = size - 1; col >= 0; col--) {
                Cell curCell = newBoard.getCell(col, row);
                if (curCell.getBall() != null) {
                    Ball b = curCell.getBall();
                    newBoard.moveToRight(b);
                }
            }
        }
        return newBoard;
    }

    public static GameBoard inclineToWest(GameBoard board) {
        int size = board.getSize();
        GameBoard newBoard = new GameBoard(board);
        newBoard.setTurnsCombination(board.getTurnsCombination() + "W");
        for (int row = 0; row <= size - 1; row++) {
            for (int col = 0; col <= size - 1; col++) {
                Cell curCell = newBoard.getCell(col, row);
                if (curCell.getBall() != null) {
                    Ball b = curCell.getBall();
                    newBoard.moveToLeft(b);
                }
            }
        }
        return newBoard;
    }

    public static GameBoard inclineToNorth(GameBoard board) {
        int size = board.getSize();
        GameBoard newBoard = new GameBoard(board);
        newBoard.setTurnsCombination(board.getTurnsCombination() + "N");
        for (int col = 0; col <= size - 1; col++) {
            for (int row = 0; row <= size - 1; row++) {
                Cell curCell = newBoard.getCell(col, row);
                if (curCell.getBall() != null) {
                    Ball b = curCell.getBall();
                    newBoard.moveToUp(b);
                }
            }
        }
        return newBoard;
    }

    public static GameBoard inclineToSouth(GameBoard board) {
        int size = board.getSize();
        GameBoard newBoard = new GameBoard(board);
        newBoard.setTurnsCombination(board.getTurnsCombination() + "S");
        for (int col = 0; col <= size - 1; col++) {
            for (int row = size - 1; row >= 0; row--) {
                Cell curCell = newBoard.getCell(col, row);
                if (curCell.getBall() != null) {
                    Ball b = curCell.getBall();
                    newBoard.moveToDown(b);
                }
            }
        }
        return newBoard;
    }
}
